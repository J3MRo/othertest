from django.db import models

class Productos(models.Model):
    id = models.BigAutoField(primary_key=True, blank=False, null=False)
    name = models.CharField(max_length=150)
    cantidad = models.SmallIntegerField()
    precio = models.DecimalField(max_digits=3, decimal_places=3)
    SKU = models.CharField(max_length=200)
    imagen = models.CharField(max_length=300)

    class Meta:
        db_table = "productos"
